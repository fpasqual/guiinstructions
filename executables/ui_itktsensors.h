/********************************************************************************
** Form generated from reading UI file 'itktsensors.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ITKTSENSORS_H
#define UI_ITKTSENSORS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ItktsensorsDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QLabel *_labelImage;

    void setupUi(QDialog *ItktsensorsDialog)
    {
        if (ItktsensorsDialog->objectName().isEmpty())
            ItktsensorsDialog->setObjectName(QString::fromUtf8("ItktsensorsDialog"));
        ItktsensorsDialog->resize(1648, 880);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/petal.png"), QSize(), QIcon::Normal, QIcon::Off);
        ItktsensorsDialog->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(ItktsensorsDialog);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        _labelImage = new QLabel(ItktsensorsDialog);
        _labelImage->setObjectName(QString::fromUtf8("_labelImage"));
        _labelImage->setMinimumSize(QSize(162, 86));
        _labelImage->setMaximumSize(QSize(1624, 856));
        _labelImage->setPixmap(QPixmap(QString::fromUtf8(":/image/petal.png")));
        _labelImage->setScaledContents(true);

        verticalLayout_2->addWidget(_labelImage);


        retranslateUi(ItktsensorsDialog);

        QMetaObject::connectSlotsByName(ItktsensorsDialog);
    } // setupUi

    void retranslateUi(QDialog *ItktsensorsDialog)
    {
        ItktsensorsDialog->setWindowTitle(QApplication::translate("ItktsensorsDialog", "ITK-Tsensors", nullptr));
        _labelImage->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ItktsensorsDialog: public Ui_ItktsensorsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ITKTSENSORS_H
