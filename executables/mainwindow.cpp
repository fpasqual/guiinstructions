#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "console.h"
#include "settingsdialog.h"
#include "ItkT.h"

#include <QWidget>
#include <QLabel>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),

    m_ui(new Ui::MainWindow),
    m_status(new QLabel),
    m_console(new Console),
    m_settings(new SettingsDialog),
    m_serial(new QSerialPort(this)),
    m_itkt(new ItkT)

{
    m_ui->setupUi(this);
    m_console->setEnabled(false);
    setCentralWidget(m_console);

    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionQuit->setEnabled(true);
    m_ui->actionConfigure->setEnabled(true);

    m_ui->statusBar->addWidget(m_status);

    initActionsConnections();

    connect(m_serial, &QSerialPort::errorOccurred, this, &MainWindow::handleError);

    //    connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::handleReadyRead);

    connect(m_console, &Console::getData, this, &MainWindow::writeData);
    m_timer.start(5000);

}

MainWindow::~MainWindow()
{
    delete m_settings;
    delete m_ui;
}


void MainWindow::handleReadyRead()
{

//       m_readData.append(m_serial->readAll());
    int check=0;
    int numTot=0;
    int numSens;
    QString tempSens;
    QByteArray data;

    if(check==0){
        writeData("on.");
    }
    check=1;



    if(m_serial->canReadLine()){
        data = m_serial->readLine();
//        qInfo()<<data;
        QString myString(data);
//        qInfo()<<myString;
//        qInfo("finita");

        if(myString.contains(QRegExp("Sensors"))){
            qDebug()<<"start";
            bool ok = false;
            QRegExp tagExp("-");
            QStringList firstList = myString.split(tagExp);
            if (!firstList.isEmpty()){
                numTot=firstList.at(0).toInt(&ok);
            }
        }
        else if(myString.contains(QRegExp("[0:9]"))){

            myString = myString.replace("\r", "");
            myString = myString.replace("\n", "");

            bool ok = false;

            QRegExp tagExp(":");
            QStringList firstList = myString.split(tagExp);

            if (!firstList.isEmpty()){
                numSens=firstList.at(0).toInt(&ok);
                tempSens=firstList.at(1);

                qDebug()<<numSens <<tempSens.toDouble()<<endl;

                m_itkt->Temperatures[numSens]->setText(tempSens);

                if(tempSens.toDouble()<23 && tempSens.toDouble()>-55){
                    m_itkt->Temperatures[numSens]->setStyleSheet("color: green;");
                }
                if(tempSens.toDouble()>=23 && tempSens.toDouble()<24){
                    m_itkt->Temperatures[numSens]->setStyleSheet("color: green;");
                }
                if(tempSens.toDouble()>=24 && tempSens.toDouble()<25){
                    m_itkt->Temperatures[numSens]->setStyleSheet("color: green");
                }
                if( tempSens.toDouble()>=25){
                    m_itkt->Temperatures[numSens]->setStyleSheet("color: green;");
                }

                if( tempSens.toDouble()<=-55){
                    m_itkt->Temperatures[numSens]->setStyleSheet("color: red");
                    m_itkt->Temperatures[numSens]->setText("ERR");

                }

            }
        }
        else if(myString.contains(QRegExp("end"))){
            qDebug()<<"end";
            check=0;

            m_serial->flush();

        }

    }






    if (!m_timer.isActive())
        m_timer.start(7000);
}

void MainWindow::openSerialPort()
{
    const SettingsDialog::Settings p = m_settings->settings();
    m_serial->setPortName(p.name);
    m_serial->setBaudRate(p.baudRate);
    m_serial->setDataBits(p.dataBits);
    m_serial->setParity(p.parity);
    m_serial->setStopBits(p.stopBits);
    m_serial->setFlowControl(p.flowControl);
    if (m_serial->open(QIODevice::ReadWrite)) {
        m_console->setEnabled(true);
        m_console->setLocalEchoEnabled(p.localEchoEnabled);
        m_ui->actionConnect->setEnabled(false);
        m_ui->actionDisconnect->setEnabled(true);
        m_ui->actionConfigure->setEnabled(false);
        m_ui->actionView->setEnabled(true);
        showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                          .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                          .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
    } else {
        QMessageBox::critical(this, tr("Error"), m_serial->errorString());

        showStatusMessage(tr("Open error"));
    }
}

void MainWindow::closeSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();
    m_console->setEnabled(false);
    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionConfigure->setEnabled(true);
    m_ui->actionView->setEnabled(true);
    showStatusMessage(tr("Disconnected"));
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Simple Terminal"),
                       tr("The <b>Simple Terminal</b> example demonstrates how to "
                          "use the Qt Serial Port module in modern GUI applications "
                          "using Qt, with a menu bar, toolbars, and a status bar."));
}

void MainWindow::writeData(const QByteArray &data)
{
    m_serial->write(data);
}


void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), m_serial->errorString());
        closeSerialPort();
    }
}

void MainWindow::initActionsConnections()
{

    connect(m_ui->actionView, &QAction::triggered, m_itkt, &ItkT::show);//da cambiare
    connect(m_ui->actionConnect, &QAction::triggered, this, &MainWindow::openSerialPort);
    connect(m_ui->actionDisconnect, &QAction::triggered, this, &MainWindow::closeSerialPort);
    connect(m_ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(m_ui->actionConfigure, &QAction::triggered, m_settings, &SettingsDialog::show);
    connect(m_ui->actionClear, &QAction::triggered, m_console, &Console::clear);
    connect(m_ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    connect(m_ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
}

void MainWindow::showStatusMessage(const QString &message)
{
    m_status->setText(message);
}
