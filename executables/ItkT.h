#ifndef ITKT_H
#define ITKT_H

#include "ui_itktsensors.h"
#include <vector>
#include <QLineEdit>
#include <QtMath>
#include <QtGui>
#include <QSlider>
#include <QMessageBox>

#include <QCoreApplication>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QStringList>
#include <QTextStream>
#include <QDialog>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QByteArray>

class QTextEdit;
class QextSerialPort;
class QSpinBox;
class QPlainTextEdit;
class QSerialPort;



class ItkT: public QDialog, private Ui_ItktsensorsDialog
{
    Q_OBJECT

public:
    ItkT();
    ~ItkT();

//int * check;
    virtual void resizeEvent( QResizeEvent *e );
    //void resizeEvent_alternative( QResizeEvent *e );
    void MySerialPort();
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &data);
    void handleError(QSerialPort::SerialPortError error);
    void showStatusMessage(const QString &message);
    void delay( int millisecondsToWait );
    QLineEdit *Temperatures[90];

    QSerialPort *serial;
public slots:





private slots:


private:
    std::vector<QLineEdit *> _Temperatures;

    std::vector<float>       _xPos, _yPos;
    //std::vector<float>     _dxFraction, _dyFraction;
    QSlider *slider;

    //    QLineEdit *message;
    //    QSpinBox *delaySpinBox;
    //    QTextEdit *received_msg;




};

#endif // ITKT_H
