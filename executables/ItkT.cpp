/* ITk-Tsensors:
   GUI displaying ITk temperature sensor readings from the Nikhef ITK
   test setup.

   Version 2.0, 19-AUG-2020
   - Second (demo) version.
*/

#include "ItkT.h"
#include <QLineEdit>
#include <QResizeEvent>
#include <QtMath>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <iostream>
#include <cstdlib>
#include <QString>
#include <QStringList>
#include <QRegExp>
#include <QList>
#include <QTime>
#include <QMessageBox>

using namespace std;


const QString VERSION( "v1.1 17-MAR-2019" );


QLineEdit *Temperatures[90];

ItkT::ItkT()
    : QDialog()
{

    setupUi( this );

    setWindowFlags( Qt::WindowMinimizeButtonHint |
                    Qt::WindowMaximizeButtonHint |
                    Qt::WindowCloseButtonHint );



    //list of T-sensor xy-coordinates
    for( int i=0; i<17; ++i ){
        if(i<8){
            Temperatures[5*i+1]=new QLineEdit( QString::number(5*i+1), _labelImage );
            Temperatures[5*i+1]->setGeometry( 770-755*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 830-740*qSin(M_PI/15 *(i))-30*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+1]);
            _xPos.push_back( 770-755*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i))  );
            _yPos.push_back( 830-740*qSin(M_PI/15 *(i))-30*qCos(M_PI/15 *(i)));
            Temperatures[5*i+1]->setReadOnly( true );

            Temperatures[5*i+2]=new QLineEdit( QString::number(5*i+2), _labelImage );
            Temperatures[5*i+2]->setGeometry( 810-755*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 810-740*qSin(M_PI/15 *(i))-60*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+2]);
            _xPos.push_back( 810-755*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i))  );
            _yPos.push_back(  810-740*qSin(M_PI/15 *(i))-60*qCos(M_PI/15 *(i)));
            Temperatures[5*i+2]->setReadOnly( true );

            Temperatures[5*i+3]=new QLineEdit( QString::number(5*i+3), _labelImage );
            Temperatures[5*i+3]->setGeometry( 800-690*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 830-680*qSin(M_PI/15 *(i))-60*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+3]);
            _xPos.push_back(800-690*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i))  );
            _yPos.push_back( 830-680*qSin(M_PI/15 *(i))-60*qCos(M_PI/15 *(i)));
            Temperatures[5*i+3]->setReadOnly( true );


            Temperatures[5*i+4]=new QLineEdit( QString::number(5*i+4), _labelImage );
            Temperatures[5*i+4]->setGeometry( 800-580*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 830-520*qSin(M_PI/15 *(i))-60*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+4]);
            _xPos.push_back( 800-580*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)) );
            _yPos.push_back( 830-520*qSin(M_PI/15 *(i))-60*qCos(M_PI/15 *(i)));
            Temperatures[5*i+4]->setReadOnly( true );


            Temperatures[5*i+5]=new QLineEdit( QString::number(5*i+5), _labelImage );
            Temperatures[5*i+5]->setGeometry( 800-450*qCos(M_PI/15 *(i))+5*qSin(M_PI/15 *(i)), 830-380*qSin(M_PI/15 *(i))-70*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+5]);
            _xPos.push_back( 800-450*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i))  );
            _yPos.push_back( 830-380*qSin(M_PI/15 *(i))-70*qCos(M_PI/15 *(i)));
            Temperatures[5*i+5]->setReadOnly( true );

        }
        if(i>7) {
            Temperatures[5*i+1]=new QLineEdit( QString::number(5*i+1), _labelImage );
            Temperatures[5*i+1]->setGeometry( 770-720*qCos(M_PI/15 *(i))+5*qSin(M_PI/15 *(i)), 830-730*qSin(M_PI/15 *(i))+90*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+1]);
            _xPos.push_back(  770-720*qCos(M_PI/15 *(i))+5*qSin(M_PI/15 *(i)) );
            _yPos.push_back( 830-730*qSin(M_PI/15 *(i))+90*qCos(M_PI/15 *(i)));
            Temperatures[5*i+1]->setReadOnly( true );

            Temperatures[5*i+2]=new QLineEdit( QString::number(5*i+2), _labelImage );
            Temperatures[5*i+2]->setGeometry( 810-700*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 810-740*qSin(M_PI/15 *(i))+15*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+2]);
            _xPos.push_back(810-700*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)) );
            _yPos.push_back(810-740*qSin(M_PI/15 *(i))+15*qCos(M_PI/15 *(i)));
            Temperatures[5*i+2]->setReadOnly( true );

            Temperatures[5*i+3]=new QLineEdit( QString::number(5*i+3), _labelImage );
            Temperatures[5*i+3]->setGeometry( 800-610*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 830-680*qSin(M_PI/15 *(i))+60*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+3]);
            _xPos.push_back( 800-610*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i))  );
            _yPos.push_back( 830-680*qSin(M_PI/15 *(i))+60*qCos(M_PI/15 *(i)));
            Temperatures[5*i+3]->setReadOnly( true );


            Temperatures[5*i+4]=new QLineEdit( QString::number(5*i+4), _labelImage );
            Temperatures[5*i+4]->setGeometry( 800-500*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i)), 830-520*qSin(M_PI/15 *(i))+60*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+4]);
            _xPos.push_back( 800-500*qCos(M_PI/15 *(i))+10*qSin(M_PI/15 *(i))  );
            _yPos.push_back( 830-520*qSin(M_PI/15 *(i))+60*qCos(M_PI/15 *(i)));
            Temperatures[5*i+4]->setReadOnly( true );

            Temperatures[5*i+5]=new QLineEdit( QString::number(5*i+5), _labelImage );
            Temperatures[5*i+5]->setGeometry( 800-410*qCos(M_PI/15 *(i))-20*qSin(M_PI/15 *(i)), 830-380*qSin(M_PI/15 *(i))+60*qCos(M_PI/15 *(i)), 40, 20 );
            _Temperatures.push_back(Temperatures[5*i+5]);
            _xPos.push_back( 800-410*qCos(M_PI/15 *(i))-20*qSin(M_PI/15 *(i)) );
            _yPos.push_back( 830-380*qSin(M_PI/15 *(i))+60*qCos(M_PI/15 *(i)));
            Temperatures[5*i+5]->setReadOnly( true );

        }
    }

    //-------------//
    Q_FOREACH(QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
        qDebug()<<port.portName();
    }

}
// ----------------------------------------------------------------------------

void ItkT::MySerialPort(){

    serial = new QSerialPort();
//    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));
    openSerialPort();

}


void ItkT::openSerialPort()
{

    serial->setPortName("cu.usbmodem143301");
    serial->open(QIODevice::ReadWrite);
    serial->setDataTerminalReady(true);
    serial->setRequestToSend(true);


    qDebug()<<serial->isOpen();
    qDebug()<<serial->isReadable();




    if(!serial->open(QIODevice::ReadWrite)){
        qDebug()<<serial->error();
        return;}
    serial->setBaudRate(9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    QMessageBox msgBox;
    if(!serial->isOpen())
    {
        QMessageBox::warning(this, "port error", "Impossible to open the port!");
        qDebug()<< serial->error();
    }
    if(serial->isOpen())
    {
        msgBox.setText("Port opened! Ready to execute");
        msgBox.exec();
    }
}

void ItkT::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();

    showStatusMessage(tr("Disconnected"));
}

void ItkT::writeData(const QByteArray &data)
{
    serial->write(data);

}






void ItkT::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        closeSerialPort();
    }
}


void ItkT::showStatusMessage(const QString &message)
{
    qDebug() << message;
}


ItkT::~ItkT(){


}

// ----------------------------------------------------------------------------

void ItkT::resizeEvent( QResizeEvent *e )
{
    int w = _labelImage->width();
    int h = _labelImage->height();

    QSize old_sz = e->oldSize();
    if( !old_sz.isValid() )
    {
        // Now is the time to set the proper original T-sensor widget positions
        // (as a fraction of the original label-with-image width and height)
        for( int i = 0; i<_Temperatures.size(); ++i )
        {
            _xPos[i] /= (float) w;
            _yPos[i] /= (float) h;
        }

        return;
    }

    for( int i = 0; i<_Temperatures.size(); ++i )
    {
        QRect r = _Temperatures[i]->geometry();

        r.moveTo( (int) (_xPos[i]*w + 0.5),
                  (int) (_yPos[i]*h + 0.5) );

        _Temperatures[i]->setGeometry( r );
    }
}


// ----------------------------------------------------------------------------

void ItkT::delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}
// ----------------------------------------------------------------------------



