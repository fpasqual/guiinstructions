
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QLineEdit>
#include <QTimer>


class QLabel;

namespace Ui {
class MainWindow;
}

class ItkT;
class Console;
class SettingsDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
//   QLineEdit *Temperatures[90];

private slots:
    void handleReadyRead();

    void openSerialPort();
    void closeSerialPort();
    void about();
    void writeData(const QByteArray &data);

    void handleError(QSerialPort::SerialPortError error);

private:
    void initActionsConnections();
    QTimer m_timer;
    QByteArray m_readData;



private:
    void showStatusMessage(const QString &message);

    Ui::MainWindow *m_ui = nullptr;
    QLabel *m_status = nullptr;
    Console *m_console = nullptr;
    SettingsDialog *m_settings = nullptr;
    QSerialPort *m_serial = nullptr;
    ItkT *m_itkt = nullptr;
};

#endif // MAINWINDOW_H
